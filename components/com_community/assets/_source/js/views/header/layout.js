// -----------------------------------------------------------------------------
// views/header/layout
// -----------------------------------------------------------------------------

// dependencies
// ------------
define([
	'sandbox',
	'views/base'
],

// definition
// ----------
function( $, BaseView ) {

	return BaseView.extend({

		render: function() {
			var showCoverButtons = $.mobile;
			if ( !showCoverButtons )
				return;

			var cover = $( '.js-focus-cover' );
			if ( !cover.length )
				return;

			var narrowScreen = cover.width() <= 480,
				buttons = $( '.js-focus-change-cover' );

			if ( narrowScreen )
				buttons.find('span').remove();

			buttons.css({
				display: 'block',
				zIndex: 20000
			});
		}

	});

});
