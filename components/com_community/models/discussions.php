<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die('Restricted access');

require_once ( JPATH_ROOT .'/components/com_community/models/models.php');

class CommunityModelDiscussions extends JCCModel
{
	/**
	 * Configuration data
	 *
	 * @var object	JPagination object
	 **/
	var $_pagination	= '';

	/**
	 * Configuration data
	 *
	 * @var object	JPagination object
	 **/
	var $total			= '';

	/**
	 * Constructor
	 */
	public function CommunityModelDiscussions()
	{
		parent::JCCModel();

		$mainframe	= JFactory::getApplication();
		$jinput 	= $mainframe->input;

		// Get pagination request variables
 	 	$limit		= ($mainframe->getCfg('list_limit') == 0) ? 5 : $mainframe->getCfg('list_limit');
	    $limitstart = $jinput->request->get('limitstart', 0, 'INT'); //JRequest::getVar('limitstart', 0, 'REQUEST');

	    if(empty($limitstart))
 	 	{
 	 		$limitstart = $jinput->get('start', 0, 'INT');
 	 	}

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/**
	 * Method to get a pagination object for the events
	 *
	 * @access public
	 * @return integer
	 */
	public function getPagination()
	{
		return $this->_pagination;
	}

	/**
	 * Get list of discussion topics
	 *
	 * @param	$id	The group id
	 * @param	$limit Limit
	 **/
	public function getDiscussionTopics( $groupId , $limit = 0 , $order = '' )
	{
		$db			= $this->getDBO();
		$limit		= ($limit == 0) ? $this->getState( 'limit' ) : $limit;
		$limitstart	= $this->getState( 'limitstart' );

		$query	= 'SELECT COUNT(*) FROM ' . $db->quoteName('#__community_groups_discuss') . ' '
				. 'WHERE ' . $db->quoteName( 'groupid' ) . '=' . $db->Quote( $groupId )
				. 'AND ' . $db->quoteName('parentid') .'=' . $db->Quote( '0' );

		$db->setQuery( $query );
		$total	= $db->loadResult();
		$this->total	= $total;
		
		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		if( empty($this->_pagination) )
		{
			jimport('joomla.html.pagination');

			$this->_pagination	= new JPagination( $total , $limitstart , $limit);
		}

		$orderByQuery	= '';
		switch( $order )
		{
			default:
				$orderByQuery = 'ORDER BY a.' . $db->quoteName('lastreplied') .' DESC ';
				break;
		}

		$query		= 'SELECT a.*, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage , b.' . $db->quoteName('post_by') .' AS lastmessageby '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' WHERE a.' . $db->quoteName('groupid').'=' . $db->Quote( $groupId )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
					. ' GROUP BY a.' . $db->quoteName('id')
					. $orderByQuery
					. 'LIMIT ' . $limitstart . ',' . $limit;

		$db->setQuery( $query );
		$result	= $db->loadObjectList();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}

	/**
	 * Method to get the last replier information from specific discussion
	 *
	 * @params $discussionId	The specific discussion row id
	 **/
	public function getLastReplier( $discussionId )
	{
		$db		= $this->getDBO();

		$query	= 'SELECT * FROM ' . $db->quoteName( '#__community_wall' ) . ' '
				. 'WHERE ' . $db->quoteName( 'contentid' ) . '=' . $db->Quote( $discussionId ) . ' '
				. 'AND ' . $db->quoteName( 'type' ) . '=' . $db->Quote( 'discussions' )
				. 'ORDER BY ' . $db->quoteName('date').' DESC LIMIT 1';
		$db->setQuery( $query );
		$result	= $db->loadObject();

		if($db->getErrorNum())
		{
			JError::raiseError( 500, $db->stderr());
		}

		return $result;
	}

	public function getRepliers( $discussionId , $groupId )
	{
		$db		= JFactory::getDBO();
		$query	= 'SELECT DISTINCT(a.' . $db->quoteName('post_by').') FROM ' . $db->quoteName( '#__community_wall' ) . ' AS a '
				. ' INNER JOIN ' . $db->quoteName('#__community_groups_members').' AS b '
				. ' ON b.' . $db->quoteName('groupid').'=' . $db->Quote( $groupId )
				. ' WHERE a.' . $db->quoteName( 'contentid' ) . '=' . $db->Quote( $discussionId )
				. ' AND a.' . $db->quoteName( 'type' ) . '=' . $db->Quote( 'discussions' )
				. ' AND a.' . $db->quoteName('post_by').'=b.' . $db->quoteName('memberid');

		$db->setQuery( $query );
		return $db->loadColumn();
	}

	/**
	 * Return a list of discussion replies.
	 *
	 * @param	int		$topicId	The replies for specific topic id.
	 * @return	Array	An array of database objects.
	 **/
	public function getReplies( $topicId )
	{
		$db		= JFactory::getDBO();

		$query	= 'SELECT a.* , b.' . $db->quoteName('name').' FROM ' . $db->quoteName('#__community_wall').' AS a '
				. ' INNER JOIN ' . $db->quoteName('#__users').' AS b '
				. ' WHERE b.' . $db->quoteName('id').'=a.' . $db->quoteName('post_by')
				. ' AND a.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
				. ' AND a.' . $db->quoteName('contentid').'=' . $db->Quote( $topicId )
				. ' ORDER BY a.' . $db->quoteName('date').' DESC ';

		$db->setQuery( $query );

		if($db->getErrorNum())
		{
			JError::raiseError(500, $db->stderr());
		}

		$result	= $db->loadObjectList();

		return $result;
	}

	/*
	 * @since 2.4
	 * @param keywords : array of keyword to be searched to match the title
	 * @param exclude : exclude the id of the discussion
	 */
	public function getRelatedDiscussion( $keywords = array(''), $exclude = array(''))
	{
		$db	= $this->getDBO();
		$topicid = JRequest::getInt('topicid',0);
        $matchQuery = '';
		$excludeQuery = '';

		if(!empty($keywords))
		{
			foreach($keywords as $words)
			{
				$matchQuery .= '  a.' . $db->quoteName('title').' LIKE '.$db->Quote( '%'.$words.'%' ). ' OR ';
			}
			$matchQuery = ' AND ('.$matchQuery.' 0 ) ';
		}

		if(!empty($exclude))
		{
			foreach($exclude as $id)
			{
				$excludeQuery .= '  a.' . $db->quoteName('id').' <> '.$db->Quote( $id ). ' AND ';
			}
			$excludeQuery = ' AND ('.$excludeQuery.' 1 ) ';
		}

		$query		= 'SELECT a.*,d.name as group_name, COUNT( b.' . $db->quoteName('id').' ) AS count, b.' . $db->quoteName('comment') .' AS lastmessage '
					. ' FROM ' . $db->quoteName( '#__community_groups_discuss' ) . ' AS a '
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS b ON b.' . $db->quoteName('contentid') .'=a.' . $db->quoteName('id')
					. ' AND b.' . $db->quoteName('date') .'=( SELECT max( date ) FROM ' . $db->quoteName('#__community_wall').' WHERE ' . $db->quoteName('contentid').'=a.' . $db->quoteName('id').' ) '
					. ' AND b.' . $db->quoteName('type').'=' . $db->Quote( 'discussions' )
					. ' LEFT JOIN ' . $db->quoteName( '#__community_wall' ) . ' AS c ON c.' . $db->quoteName('contentid').'=a.' . $db->quoteName('id')
					. ' AND c.' . $db->quoteName('type').'=' . $db->Quote( 'discussions')
					. ' LEFT JOIN ' . $db->quoteName( '#__community_groups' ) . ' AS d ON a.' . $db->quoteName('groupid').'=d.' . $db->quoteName('id')
					. ' WHERE a.' . $db->quoteName('lock').'=' . $db->Quote( '0' )
					. ' AND d.' . $db->quoteName('approvals').'=' . $db->Quote( '0' )
					. ' AND a.' . $db->quoteName('parentid').'=' . $db->Quote( '0' )
                    . ' AND a.' . $db->quoteName('id').'!=' . $db->Quote($topicid)
					. ' AND d.' . $db->quoteName('published') . '=' .$db->Quote('1')
					. $matchQuery
					. $excludeQuery
					. ' GROUP BY a.' . $db->quoteName('id');

		$db->setQuery( $query );

		if($db->getErrorNum())
		{
			JError::raiseError(500, $db->stderr());
		}

		$result	= $db->loadObjectList();

		return $result;
	}

	public function getSiteDiscussionCount()
	{
		$db	= $this->getDBO();

		$sql = 'SELECT COUNT(*) FROM '. $db->quoteName('#__community_groups_discuss');

		$db->setQuery($sql);

		$result = $db->loadResult();

		return $result;
	}
	
	/**
	 * Method to get the record form.
	 *
	 * @param   array      $data        Data for the form.
	 * @param   boolean    $loadData    True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_community.discussion', 'discussion', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}
		$jinput = JFactory::getApplication()->input;

		// The front end calls this model and uses a_id to avoid id clashes so we need to check for that first.
		if ($jinput->get('a_id'))
		{
			$id = $jinput->get('a_id', 0);
		}
		// The back end uses id so we use that the rest of the time and set it to 0 by default.
		else
		{
			$id = $jinput->get('id', 0);
		}
		// Determine correct permissions to check.
		if ($this->getState('article.id'))
		{
			$id = $this->getState('article.id');
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit');
			// Existing record. Can only edit own articles in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit.own');
		}
		else
		{
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.create');
		}

		$user = JFactory::getUser();

		// Check for existing article.
		// Modify the form based on Edit State access controls.
		if ($id != 0 && (!$user->authorise('core.edit.state', 'com_content.article.' . (int) $id))
			|| ($id == 0 && !$user->authorise('core.edit.state', 'com_content'))
		)
		{
			// Disable fields for display.
			$form->setFieldAttribute('featured', 'disabled', 'true');
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is an article you can edit.
			$form->setFieldAttribute('featured', 'filter', 'unset');
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');
		}

		// Prevent messing with article language and category when editing existing article with associations
		$app = JFactory::getApplication();
		$assoc = JLanguageAssociations::isEnabled();

		if ($app->isSite() && $assoc && $this->getState('article.id'))
		{
			$form->setFieldAttribute('language', 'readonly', 'true');
			$form->setFieldAttribute('catid', 'readonly', 'true');
			$form->setFieldAttribute('language', 'filter', 'unset');
			$form->setFieldAttribute('catid', 'filter', 'unset');
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$app = JFactory::getApplication();
		$data = $app->getUserState('com_community.edit.discussion.data', array());
		
		if (empty($data))
		{
			$data = $this->getItem();
			//print_r($data);die;
			// Prime some default values.
		}

		//$this->preprocessData('com_newsfeeds.newsfeed', $data);


		return $data;
	}
	
	/**
	 * Method to get a form object.
	 *
	 * @param	string		$name		The name of the form.
	 * @param	string		$source		The form source. Can be XML string if file flag is set to false.
	 * @param	array		$options	Optional array of options for the form creation.
	 * @param	boolean		$clear		Optional argument to force load a new form.
	 * @param	string		$xpath		An optional xpath to search for the fields.
	 * @return	mixed		JForm object on success, False on error.
	 */
	protected function loadForm($name, $source = null, $options = array(), $clear = false, $xpath = false)
	{
		// Handle the optional arguments.
		$options['control']	= JArrayHelper::getValue($options, 'control', false);

		// Create a signature hash.
		$hash = md5($source.serialize($options));

		// Check if we can use a previously loaded form.
		if (isset($this->_forms[$hash]) && !$clear) {
			return $this->_forms[$hash];
		}

		// Get the form.
		JForm::addFormPath(JPATH_COMPONENT.'/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT.'/models/fields');

		try {
			$form = JForm::getInstance($name, $source, $options, false, $xpath);

			if (isset($options['load_data']) && $options['load_data']) {
				// Get the data for the form.
				$data = $this->loadFormData();
			} else {
				$data = array();
			}

			// Allow for additional modification of the form, and events to be triggered.
			// We pass the data because plugins may require it.
			$this->preprocessForm($form, $data);

			// Load the data into the form after the plugins have operated.
			$form->bind($data);

		} catch (Exception $e) {
			$this->setError($e->getMessage());
			return false;
		}

		// Store the form for later.
		$this->_forms[$hash] = $form;

		return $form;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param   integer    The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 */
	public function getItem($pk = null)
	{

		// Load associated content items
		$app = JFactory::getApplication();
		$assoc = JLanguageAssociations::isEnabled();
		$item = new StdClass();
		if ($assoc)
		{
			$item->associations = array();

			if ($item->id != null)
			{
				$associations = JLanguageAssociations::getAssociations('com_content', '#__content', 'com_content.item', $item->id);

				foreach ($associations as $tag => $association)
				{
					$item->associations[$tag] = $association->id;
				}
			}
		}
		
		$jinput = JFactory::getApplication()->input;
		
		$item->tags = new JHelperTags;
		$item->tags->getTagIds($jinput->get('topicid'), 'com_community.groups');
		
		return $item;
	}
	
	protected function preprocessForm(JForm $form, $data, $group = 'content')
	{
		// Association content items
		$app = JFactory::getApplication();
		$assoc = JLanguageAssociations::isEnabled();
		if ($assoc)
		{
			$languages = JLanguageHelper::getLanguages('lang_code');

			// force to array (perhaps move to $this->loadFormData())
			$data = (array) $data;

			$addform = new SimpleXMLElement('<form />');
			$fields = $addform->addChild('fields');
			$fields->addAttribute('name', 'associations');
			$fieldset = $fields->addChild('fieldset');
			$fieldset->addAttribute('name', 'item_associations');
			$fieldset->addAttribute('description', 'COM_CONTENT_ITEM_ASSOCIATIONS_FIELDSET_DESC');
			$add = false;
			foreach ($languages as $tag => $language)
			{
				if (empty($data['language']) || $tag != $data['language'])
				{
					$add = true;
					$field = $fieldset->addChild('field');
					$field->addAttribute('name', $tag);
					$field->addAttribute('type', 'modal_article');
					$field->addAttribute('language', $tag);
					$field->addAttribute('label', $language->title);
					$field->addAttribute('translate_label', 'false');
					$field->addAttribute('edit', 'true');
					$field->addAttribute('clear', 'true');
				}
			}
			if ($add)
			{
				$form->load($addform, false);
			}
		}

		//parent::preprocessForm($form, $data, $group);
	}
}
