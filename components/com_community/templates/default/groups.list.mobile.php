<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>
<h3>My Groups</h3>

<?php

if( $groups )
{
	for( $i = 0; $i < count( $groups ); $i++ )
	{
		$group	=& $groups[$i]; 
?>

<div class="group-list">

	<div class="group-avatar">
		<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );?>">
			<img class="cAvatar" src="<?php echo $group->getThumbAvatar();?>" alt="<?php echo $this->escape($group->name); ?>"/>
		</a>
	</div>
	
	
	<div class="group-detail">
	
		<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=groups&task=viewgroup&groupid=' . $group->id );?>">
			<?php echo $this->escape($group->name); ?>
		</a>

	<div class="groupDescription"><?php echo $this->escape($group->description); ?></div>
	
	<div class="groupCreated small"><?php echo JText::sprintf('COM_COMMUNITY_GROUPS_CREATE_TIME_ON' , JHTML::_('date', $group->created, JText::_('DATE_FORMAT_LC')) );?></div>
	

	</div>
	
	<div class="clear"></div>

</div><!--end of group list-->

<div class="group-list-action">

	<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=groups&task=viewmembers&groupid=' . $group->id ); ?>">
		<?php echo JText::sprintf((CStringHelper::isPlural($group->membercount)) ? 'COM_COMMUNITY_GROUPS_MEMBER_COUNT_MANY':'COM_COMMUNITY_GROUPS_MEMBER_COUNT', $group->membercount);?>
	</a>
	
	<?php echo JText::sprintf((CStringHelper::isPlural($group->discusscount)) ? 'COM_COMMUNITY_GROUPS_DISCUSSION_COUNT_MANY' :'COM_COMMUNITY_GROUPS_DISCUSSION_COUNT', $group->discusscount);?>

	<?php echo JText::sprintf((CStringHelper::isPlural($group->wallcount)) ? 'COM_COMMUNITY_GROUPS_WALL_COUNT_MANY' : 'COM_COMMUNITY_GROUPS_WALL_COUNT', $group->wallcount);?>


</div><!--end of group list action-->



	<div class="community-groups-results-item">
		<div class="community-groups-results-right">
			<div class="groupActions">
				<?php
				if( $isCommunityAdmin && $showFeatured )
				{
					if( !in_array($group->id, $featuredList) )
					{
				?>
					<span class="jsIcon1 icon-addfeatured" style="margin-right: 5px;">	            
			            <a onclick="joms.featured.add('<?php echo $group->id;?>','groups');" href="javascript:void(0);">	            	            
			            <?php echo JText::_('COM_COMMUNITY_MAKE_FEATURED'); ?>
			            </a>
			        </span>
				<?php			
					}
				}
				?>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
<?php
	}
}
else
{
?>
	<div class="group-not-found"><?php echo JText::_('COM_COMMUNITY_GROUPS_NOITEM'); ?></div>
<?php
}
?>

<?php if (!is_null($pagination)) {?>
<div class="pagination-container">
	<?php echo $pagination->getPagesLinks(); ?>
</div>
<?php } ?>