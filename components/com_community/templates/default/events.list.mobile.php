<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();

?>


<h3>My Events</h3>

<?php

if( $events )
{
	for( $i = 0; $i < count( $events ); $i++ )
	{
		$event				=& $events[$i];
		$creator			= CFactory::getUser($event->creator);
		$creatorUtcOffset	= $creator->getUtcOffset();
?>




<div class="event-list">

<div class="event-avatar">
	<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id );?>">
		<img class="cAvatar" src="<?php echo $event->getThumbAvatar();?>" border="0" alt="<?php echo $this->escape($event->title); ?>"/>
	</a>
</div>

<div class="event-detail">
	<h4 class="eventName">
		<a href="<?php echo CRoute::_( 'index.php?option=com_community&view=events&task=viewevent&eventid=' . $event->id );?>"><?php echo $this->escape($event->title); ?></a>
	</h4>

	<div class="eventLocation">
		Location: <?php echo $this->escape($event->location);?>
	</div>

	<div class="eventTime">
		<?php echo JText::sprintf('COM_COMMUNITY_EVENTS_DURATION', JHTML::_('date', $event->startdate, JText::_('DATE_FORMAT_LC2') , $creatorUtcOffset ), JHTML::_('date', $event->enddate, JText::_('DATE_FORMAT_LC2') , $creatorUtcOffset )); ?>
	</div>
	
		<?php if($isExpired) { ?> 
		    <span class="icon-offline-overlay">Status: <?php echo JText::_('COM_COMMUNITY_EVENTS_PAST'); ?>&nbsp;</span>
		<?php } else if(CEventHelper::isToday($event)) { ?>
			<span class="icon-online-overlay">Status: <?php echo JText::_('COM_COMMUNITY_EVENTS_ONGOING'); ?>&nbsp;</span>
		<?php }?>


</div>

<div class="clear"></div>

</div><!--end of event-list-->

	<div class="eventActions">
		<span class="jsIcon1 icon-group">
			<a href="<?php echo CRoute::_('index.php?option=com_community&view=events&task=viewguest&eventid=' . $event->id . '&type='.COMMUNITY_EVENT_STATUS_ATTEND);?>"><?php echo JText::sprintf((cIsPlural($event->confirmedcount)) ? 'COM_COMMUNITY_EVENTS_MANY_GUEST_COUNT':'COM_COMMUNITY_EVENTS_GUEST_COUNT', $event->confirmedcount);?></a>. Attending?
		</span>
	</div>




<?php
	}
} else {
?>
	<div class="event-not-found"><?php echo JText::_('COM_COMMUNITY_EVENTS_NO_EVENTS_ERROR'); ?></div>
<?php } ?>

<?php if (!is_null($pagination)) {?>
<div class="pagination-container">
	<?php echo $pagination->getPagesLinks(); ?>
</div>
<?php }?>