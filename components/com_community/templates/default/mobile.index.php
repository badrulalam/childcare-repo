<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
$uri	= CRoute::_('index.php?option=com_community&screen=mobile' , false );
$uri	= base64_encode($uri);


$template = new CTemplateHelper();
$style = $template->getTemplateAsset('style.mobile', 'css');
$style = $style->url;

?>

<script type="text/javascript">
<!--
function switchMenu(obj) {
	var el = document.getElementById(obj);
	if ( el.style.display != "none" ) {
		el.style.display = 'none';
	}
	else {
		el.style.display = '';
	}
}
//-->
</script>

<!--$uri	= CRoute::_('index.php?option=com_community&screen=mobile' , true );-->

<!--<link href="<?php $style ?>"/>-->

<!--<link rel="stylesheet" type="text/css" href="<?php $style ?>/components/com_community/templates/default/css/style.mobile.css" />-->




<div id="community-wrap">


<img src="../mobile-images/home.png" alt="" />

	<div id="community-header">
		<!--<h1>
			<a href="<?php echo CRoute::_('index.php?option=com_community'); ?>" title="<?php echo $config->getValue('sitename'); ?>" target="page">
				<span><?php echo $config->getValue('sitename'); ?></span>
			</a>
			</h1>-->
			
			<div class="community-header-top">
			<h1 class="sitename"><?php echo $config->getValue('sitename'); ?></h1>
			
					<div class="clear"></div>
	
			
			</div><!--end of community header top-->
			
			
		<?php echo $toolbar ?>
	</div>
	

	<div id="community-content">
		<?php echo $content ?>

	</div>
	

	
	<div id="community-footer">
		<?php echo $switcher ?>

	</div>


<div id="mobile-footer">
		<form action="index.php" method="post" style="text-align: center;">
			<input type="hidden" name="option" value="<?php echo COM_USER_NAME;?>" />
			<input type="hidden" name="task" value="<?php echo COM_USER_TAKS_LOGOUT;?>" />
			<!--<input type="hidden" name="return" value="<?php echo $uri ?>" />-->
			<input type="submit" value="Logout" class="logout"/>
		</form>

</div>
	
</div><!--end of community wrap-->



