<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('_JEXEC') or die();
?>




<div id="community-events-wrap">
	<?php if ( $index ) : ?>
	<div class="cRow">
		<div class="ctitle"><?php echo JText::_('COM_COMMUNITY_CATEGORIES');?></div>
		<ul class="cResetList c3colList">
		<?php if( $categories ): ?>
		<li>
			<a href="<?php echo CRoute::_('index.php?option=com_community&view=events');?>"><?php echo JText::_( 'COM_COMMUNITY_EVENTS_ALL' ); ?> </a>
		</li>
		<?php foreach( $categories as $row ): ?>
		<li>
			<a href="<?php echo CRoute::_('index.php?option=com_community&view=events&categoryid=' . $row->id ); ?>"><?php echo JText::_( $this->escape($row->name) ); ?></a> ( <?php echo $row->count; ?> )
		</li>
		<?php endforeach; ?>
		<?php else: ?>
			<li><?php echo JText::_('COM_COMMUNITY_GROUPS_CATEGORY_NOITEM'); ?></li>
		<?php endif; ?>
		</ul>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

	
	<?php echo $sortings; ?>
	
	<?php if( $index ): ?>
	<h3>
		<?php echo ( isset($category) && ($category->id != '0') ) ? JText::sprintf('COM_COMMUNITY_GROUPS_CATEGORY_NAME' , JText::_( $this->escape($category->name) ) ) : JText::_( 'COM_COMMUNITY_EVENTS_ALL' ); ?>
	</h3>
	<?php endif; ?>

	<?php echo $eventsNearbyHTML;?>

	<div id="community-events-results-wrapper" class="cMain clrfix">
		<?php echo $eventsHTML;?>
	</div>
</div>