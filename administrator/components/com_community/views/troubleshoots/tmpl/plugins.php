<table class="table table-condensed">
    <thead>
        <tr>
            <th>Id</th>
            <th>Ordering</th>
            <th>Folder</th>
            <th>Name</th>
            <th>Element</th>
            <th>Version</th>
            <th>Developer</th>
            <th>Enabled</th>                  
        </tr>
    </thead>
    <tbody>
        <?php $communityPlugins = $this->troubleshoots->extensions['plugins']; ?>
        <?php foreach ($this->plugins as $plugin) { ?>
            <?php
            $plugin->manifest_cache = new JRegistry($plugin->manifest_cache);
            $language = JFactory::getLanguage();
            $language->load('plg_' . $plugin->folder . '_' . $plugin->element);
            ?>
            <tr class="<?php echo (in_array($plugin->element, $communityPlugins[$plugin->folder])) ? '' : 'error'; ?>">
                <td>
                    <?php echo $plugin->extension_id; ?>
                </td>
                <td>
                    <?php echo $plugin->ordering; ?>
                </td>
                <td>
                    <?php echo $plugin->folder; ?>
                </td>
                <td>
                    <?php echo JText::_($plugin->name); ?>
                </td>              
                <td>
                    <?php echo $plugin->element; ?>
                </td>
                <td>
                    <?php echo $plugin->manifest_cache->get('version'); ?>
                </td>
                <td>
                    <?php echo (in_array($plugin->element, $communityPlugins[$plugin->folder])) ? 'JomSocial' : $plugin->manifest_cache->get('author'); ?>
                </td>
                <td>
                    <?php if ($plugin->enabled) : ?>
                        <i class="icon-publish"></i>
                    <?php else : ?>
                        <i class="icon-unpublish"></i>
                    <?php endif; ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>