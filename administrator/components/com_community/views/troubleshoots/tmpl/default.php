<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<!-- Tabs header -->
<ul id="myTab" class="nav nav-tabs">   
    <li class="active"><a href="#requirements" data-toggle="tab">System requirements</a></li>    
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Check File Modifications<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="#files-frontend" data-toggle="tab">@Frontend</a></li>
            <li><a href="#files-backend" data-toggle="tab">@Backend</a></li>            
        </ul>
    </li>
    <li class=""><a href="#jQuery" data-toggle="tab">jQuery</a></li>
    <li class=""><a href="#plugins" data-toggle="tab">Plugins Installed</a></li>
</ul>
<!-- Tabs content -->
<div id="myTabContent" class="tab-content">
    <!-- System requirements -->
    <div class="tab-pane fade active in" id="requirements">
        <?php include_once 'systemrequirements.php'; ?>
    </div>
    <!-- Core files checking -->
    <div class="tab-pane fade" id="files-frontend">
        <?php $this->troubleshoots->coreFilesCheck(JPATH_ROOT . '/components/com_community'); ?>
    </div>
    <!-- Core files checking -->
    <div class="tab-pane fade" id="files-backend">
        <?php $this->troubleshoots->coreFilesCheck(JPATH_ADMINISTRATOR . '/components/com_community'); ?>
    </div>
    <!-- jQuery detection -->
    <div class="tab-pane fade" id="jQuery">
        <?php $this->troubleshoots->filesCheck(JPATH_ROOT); ?>
    </div>
    <!-- Extensions -->
    <div class="tab-pane fade" id="plugins">
        <?php include_once 'plugins.php'; ?>
    </div>

</div>

