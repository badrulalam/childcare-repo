<?php

/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

if (!class_exists('CTroubleshoots')) {

    class CTroubleshoots {

        public $extensions = array();

        public function __construct() {
            $communityPlugins['community'] = array(
                'allvideo',
                'editormyphotos',
                'events',
                'feeds',
                'friendslocation',
                'groups',
                'icontact',
                'input',
                'inputlink',
                'invite',
                'jsnote',
                'kunena',
                'kunenagroups',
                'kunenamenu',
                'latestphoto',
                'log',
                'myarticles',
                'myblog',
                'myblogtoolbar',
                'mycontacts',
                'mygoogleads',
                'mykunena',
                'mytaggedvideos',
                'myvideos',
                'nicetalk',
                'system',
                'twitter',
                'walls',
                'wordfilter'
            );
            $communityPlugins['content'] = array(
                'groupdiscuss',
                'jomsocial_fb_comments',
                'jomsocial_fb_likes'
            );
            $communityPlugins['editors-xtd'] = array(
                'myphotos'
            );
            $communityPlugins['kunena'] = array(
                'community'
            );
            $communityPlugins['system'] = array(
                'azrul.system',
                'jomsocial',
                'jomsocialconnect',
                'jomsocialinprofile',
                'jomsocialredirect',
                'jomsocialupdate'
            );
            $communityPlugins['user'] = array(
                'jomsocialuser',
                'registeractivity'
            );
            $this->extensions['plugins'] = $communityPlugins;
        }

        public function coreFilesCheck($path = '.', $level = 0) {

            /* Directories to ignore when listing output. Many hosts will deny PHP access to the cgi-bin. */
            $ignore = array('cgi-bin', '.', '..');
            /* Open the directory to the handle $dh */
            $dh = @opendir($path);
            $flag = true;
            echo '<ul>';
            /* Loop through the directory */
            while (false !== ( $file = readdir($dh) )) {
                /* Check that this file is not to be ignored */
                if (!in_array($file, $ignore)) {
                    if (is_dir("$path/$file")) {
                        echo "<li><strong>$file</strong></li>";
                        /* Its a directory, so we need to keep reading down... */
                        $this->coreFilesCheck("$path/$file", ($level + 1));
                        /* Re-call this same function but on a new directory. this is what makes function recursive. */
                    } else {
                        if ($file == 'index.html')
                            $flag = false;
                        $filePath = trim(str_replace(JPATH_ROOT, '', $path) . '/' . $file);
                        $checkSum = md5_file($path . '/' . $file);
                        $hashList = $this->getHash();
                        if (!isset($hashList[$filePath])) {
                            echo '<li>' . $filePath . '<small> ' . '<span class="label">No checksum data</span></small></li>';
                        } else if ((isset($hashList[$filePath]) && $hashList[$filePath] != $checkSum)) {
                            echo '<li>' . $filePath . '<small> ' . '<span class="label label-important">Modified</span></small></li>';
                        } else {
                            echo '<li>' . $filePath . ' <i class="icon-ok"></i></li>';
                        }
                    }
                }
            }
            /* index.html checking */
            if ($flag) {
                echo '<span class="label label-important">No index.html</span>';
            }
            echo '</ul>';
            closedir($dh);
        }

        public function filesCheck($path = '.', $level = 0, $showTree = false) {

            /* Directories to ignore when listing output. Many hosts will deny PHP access to the cgi-bin. */
            $ignore = array('cgi-bin', '.', '..');
            /* Open the directory to the handle $dh */
            $dh = @opendir($path);
            if ($showTree)
                echo '<ul>';
            /* Loop through the directory */
            while (false !== ( $file = readdir($dh) )) {
                /* Check that this file is not to be ignored */
                if (!in_array($file, $ignore)) {
                    if (is_dir("$path/$file")) {
                        if ($showTree)
                            echo "<li><strong>$file</strong></li>";
                        /* Its a directory, so we need to keep reading down... */
                        $this->filesCheck("$path/$file", ($level + 1));
                        /* Re-call this same function but on a new directory. this is what makes function recursive. */
                    } else {
                        if ($file == 'index.html')
                            $flag = false;
                        $filePath = trim(str_replace(JPATH_ROOT, '', $path) . '/' . $file);
                        $content = JFile::read($path . '/' . $file);
                        if (strpos($content, 'window.jQuery = window.$ = jQuery;') !== false) {
                            echo '<li class="warning">' . $filePath . ' <small><span class="label label-warning">jQuery detected</span></small></i></li>';
                        } else {
                            //echo '<li>' . $filePath . '</li>';
                        }
                    }
                }
            }
            if ($showTree)
                echo '</ul>';
            closedir($dh);
        }

        public function getHash() {
            static $list;
            if (!isset($list)) {
                $content = JFIle::read(JPATH_COMPONENT_ADMINISTRATOR . '/hash.ini');
                $array = explode("\n", $content);
                foreach ($array as $el) {
                    $parts = explode('=', $el);
                    if (count($parts) == 2) {
                        $list[trim($parts[0])] = $parts[1];
                    }
                }
            }

            return $list;
        }

        /**
         * 
         * @return type
         */
        public function getCommunityPlugins() {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                    ->select('*')
                    ->from('#__extensions')
                    ->where('type =' . $db->quote('plugin'))
                    ->where(' ( 
                        folder = ' . $db->quote('community')
                            . ' OR folder = ' . $db->quote('content')
                            . ' OR folder = ' . $db->quote('editors-xtd')
                            . ' OR folder = ' . $db->quote('kunena')
                            . ' OR folder = ' . $db->quote('system')
                            . ' OR folder = ' . $db->quote('user')
                            . ' ) ')
                    ->order('folder')
                    ->order('element')
                    ->order('ordering');
            $db->setQuery($query);
            return $db->loadObjectList();
        }

        /**
         * @todo Load from JSON
         * @return array
         */
        public function getSystemRequirements() {
            $db = JFactory::getDbo();
            $mySQLCheck[] = array(
                'minimum' => '5.0.4',
                'recommended' => '5.0.4 or higher',
                'current' => array(version_compare($db->getVersion(), '5.0.4') >= 0, $db->getVersion()),
                'description' => 'PHP should running on Apache / nix system',
                'help' => 'Please contact your hosting provider and ask them to upgrade MySQL to the minimum required version.'
            );          
            $phpChecks[] = array(
                'minimum' => '5.3.1',
                'recommended' => '5.3 or higher',
                'current' => array(version_compare(PHP_VERSION, '5.3') >= 0, phpversion()),
                'description' => 'PHP should running on Apache / nix system',
                'help' => 'Please contact your hosting provider and ask them to upgrade MySQL to the minimum required version.'
            );
            $phpChecks[] = array(
                'minimum' => 'imagecreatefromjpeg',
                'recommended' => 'imagecreatefromjpeg',
                'current' => array(function_exists('imagecreatefromjpeg')),
                'description' => 'Supported image function',
                'help' => 'Please contact your hosting provider and ask them to install this function on your server.'
            );
            $phpChecks[] = array(
                'minimum' => 'imagecreatefrompng',
                'recommended' => 'imagecreatefrompng',
                'current' => array(function_exists('imagecreatefrompng')),
                'description' => 'Supported image function',
                'help' => 'Please contact your hosting provider and ask them to install this function on your server.'
            );
            $phpChecks[] = array(
                'minimum' => 'imagecreatefromgif',
                'recommended' => 'imagecreatefromgif',
                'current' => array(function_exists('imagecreatefromgif')),
                'description' => 'Supported image function',
                'help' => 'Please contact your hosting provider and ask them to install this function on your server.'
            );
            $phpChecks[] = array(
                'minimum' => 'imagecreatefromgd',
                'recommended' => 'imagecreatefromgd',
                'current' => array(function_exists('imagecreatefromgd')),
                'description' => 'Supported image function',
                'help' => 'Please contact your hosting provider and ask them to install this function on your server.'
            );
            $phpChecks[] = array(
                'minimum' => 'imagecreatefromgd2',
                'recommended' => 'imagecreatefromgd2',
                'current' => array(function_exists('imagecreatefromgd2')),
                'description' => 'Supported image function',
                'help' => 'Please contact your hosting provider and ask them to install this function on your server.'
            );
            $curlVersion = curl_version();
            $phpChecks[] = array(
                'minimum' => 'curl',
                'recommended' => 'curl',
                'current' => array(
                    in_array('curl', get_loaded_extensions()),
                    $curlVersion['version'] . '-' . $curlVersion['ssl_version'] . '-' . $curlVersion['libz_version']
                ),
                'description' => 'Used for Video Linking and Facebook Connect',
                'help' => 'Please contact your hosting provider and ask them to install that library on your server.'
            );
            $phpChecks[] = array(
                'minimum' => 'max_execution_time: 30',
                'recommended' => 'max_execution_time: 300',
                'current' => array(ini_get('max_execution_time') > 30, ini_get('max_execution_time')),
                'description' => 'Check your php.ini',
                'help' => 'Please open the php.ini file and change the value of this variable to the minimum requirement. If you can\'t do it yourself or if you don\'t have access to this file, please contact your hosting provider and ask them to do this for you.'
            );
            $phpChecks[] = array(
                'minimum' => 'max_input_time: 30',
                'recommended' => 'max_input_time: 300',
                'current' => array(ini_get('max_input_time') > 30, ini_get('max_input_time')),
                'description' => 'Check your php.ini',
                'help' => 'Please open the php.ini file and change the value of this variable to the minimum requirement. If you can\'t do it yourself or if you don\'t have access to this file, please contact your hosting provider and ask them to do this for you.'
            );
            $phpChecks[] = array(
                'minimum' => 'memory_limit: 128M',
                'recommended' => 'memory_limit: 1024M',
                'current' => array(ini_get('memory_limit') > 128, ini_get('memory_limit')),
                'description' => 'Check your php.ini',
                'help' => 'Please open the php.ini file and change the value of this variable to the minimum requirement. If you can\'t do it yourself or if you don\'t have access to this file, please contact your hosting provider and ask them to do this for you.'
            );
            $phpChecks[] = array(
                'minimum' => 'post_max_size: 8M',
                'recommended' => 'post_max_size: 4096M',
                'current' => array(ini_get('post_max_size') > 8, ini_get('post_max_size')),
                'description' => 'Check your php.ini',
                'help' => 'Please open the php.ini file and change the value of this variable to the minimum requirement. If you can\'t do it yourself or if you don\'t have access to this file, please contact your hosting provider and ask them to do this for you.'
            );
            $phpChecks[] = array(
                'minimum' => 'upload_max_filesize: 8M',
                'recommended' => 'upload_max_filesize: 4096M',
                'current' => array(ini_get('upload_max_filesize') > 8, ini_get('upload_max_filesize')),
                'description' => 'Check your php.ini',
                'help' => 'Please open the php.ini file and change the value of this variable to the minimum requirement. If you can\'t do it yourself or if you don\'t have access to this file, please contact your hosting provider and ask them to do this for you.'
            );
            $systemRequirements['MySQL'] = $mySQLCheck;
            $systemRequirements['PHP'] = $phpChecks;
            return $systemRequirements;
        }

    }

}