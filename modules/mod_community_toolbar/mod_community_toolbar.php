<?php
/**
 * @category	Model
 * @package		JomSocial
 * @subpackage	Groups
 * @copyright (C) 2008 by iJoomla Inc - All rights reserved!
 * @license		GNU/GPL, see LICENSE.php
 */
defined('_JEXEC') or die('Restricted access');
if(is_file(JPATH_BASE.'/components/com_community/libraries/core.php'))
{
	require_once JPATH_BASE.'/components/com_community/libraries/core.php';
    CWindow::load();

	$document = JFactory::getDocument();
	$document->addStyleSheet(rtrim(JURI::root(), '/').'/components/com_community/templates/default/css/style.css');

	$toolbar = CToolbarLibrary::getInstance();

	$my = CFactory::getUser();

	// Logout link
	$config     = CFactory::getConfig();
	$logoutLink = base64_encode(CRoute::_('index.php?option=com_community&view='.$config->get('redirect_logout'), false));

	// Menu Toolbar
	$menus = $toolbar->getItems();
	$toolbar->addLegacyToolbars($menus);

	$model      = CFactory::getModel( 'Toolbar' );
	$notifModel = CFactory::getModel('notification');


	// Notification count
	$newMessageCount      = $toolbar->getTotalNotifications( 'inbox' );
	$newEventInviteCount  = $toolbar->getTotalNotifications( 'events' );
	$newFriendInviteCount = $toolbar->getTotalNotifications( 'friends' );
	$newGroupInviteCount  = $toolbar->getTotalNotifications( 'groups' );

	$myParams             = $my->getParams();
	$newNotificationCount = $notifModel->getNotificationCount($my->id,'0',$myParams->get('lastnotificationlist',''));
	$toolbar_left_logo = $params->get('toolbar_logo','');
	$logo_url = $params->get('logo_url','');

	require( JModuleHelper::getLayoutPath('mod_community_toolbar') );
}
else
{
	echo 'This toolbar module requires JomSocial, please install it.';
}